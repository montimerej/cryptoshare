const express = require('express');
const mongodb = require('mongodb');
const { restart } = require('nodemon');

const router = express.Router();

// Get Users
router.get('/', async (req, res) => {
    const users = await loadUsersCollection();
    res.send(await users.find({}).toArray());
});

// Add Users
router.post('/', async (req, res) => {
    const users = await loadUsersCollection();
    await users.insertOne({
        "username": req.body.username,
        "email": req.body.email,
        "password": req.body.password,
        "confirm": req.body.confirm,
        createAt: new Date()
    });
    res.status(201).send();
});

//Delete Users
router.delete('/:id', async (req, res) =>{
const users = await loadUsersCollection();
await users.deleteOne({ _id: new mongodb.ObjectID(req.params.id) });
res.status(200).send();
});

async function loadUsersCollection(){
    const client = await mongodb.MongoClient.connect
('mongodb+srv://jmontimere:Monj5935$@ievangelized.qamoe.mongodb.net/cryptoshare',{
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    return client.db('cryptoshare').collection('users');
}

module.exports = router;