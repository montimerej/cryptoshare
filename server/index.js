const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require("morgan");
const mongoose = require("mongoose");
const config = require("./routes/api/loginsession/config/db");

const app = express();

//configure database and mongosse
mongoose
  .connect(config.database, { useNewUrlParser: true })
  .then(() => {
    console.log("Database is connected");
  })
  .catch(err => {
    console.log({ database_error: err });
  });

//Middleware
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

const posts = require('./routes/api/posts');
const users = require('./routes/api/user-bak');

//Handle production

if(process.env.NODE_ENV === 'producion'){
    // static folder
    app.use(expres.static(__dirname + '/public'));

    //Handle SPA
    app.get(/.*/,(req, res) => res.sendFile(__dirname + '/public/index.html'));
}

const port = process.env.PORT || 5000;

app.use('/api/posts', posts);
app.use('/api/users', users);
app.use(morgan("dev"));


app.listen(port, () => console.log(`Server started on port ${port}`));